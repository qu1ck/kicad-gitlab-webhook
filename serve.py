# This is a stub to be able to enable hot reloading for debug
from app.main import app

import uvicorn

if __name__ == '__main__':
    app.debug = True
    uvicorn.run("serve:app", host='0.0.0.0', port=5000,
                reload=True, reload_dirs=["./app"], log_config="logging.json")
